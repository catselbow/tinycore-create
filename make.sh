#!/bin/sh

set -e

if [ "$UID" = "0" ]
then
    /bin/true
else
    echo "Must use sudo or be root to run this script."
    exit
fi

# Select version number:
VERSION=15
KERNELVERSION=`wget -q -O- http://repo.tinycorelinux.net/$VERSION.x/x86_64/tcz/ | grep filesystems | awk -F- '{print $2}'`

echo '######################################################################'
echo "# Creating customized boot media for CorePure64 version $VERSION."
echo "# Using kernel version $KERNELVERSION."
echo '######################################################################'
cat extratczs-template.txt | sed -e "s/KERNELVERSION/$KERNELVERSION/g" > extratczs.txt

echo '######################################################################'
echo '# Create bootlocal.sh and shadow:'
echo '######################################################################'

echo Enter your e-mail address:
read MYEMAILADDRESS

echo Enter your smtp server:
read MYSMTPSERVER

cat bootlocal.sh.template | \
    sed -e "s/MYEMAILADDRESS/$MYEMAILADDRESS/g" | \
    sed -e "s/MYSMTPSERVER/$MYSMTPSERVER/g" > \
    bootlocal.sh

echo Enter password for tc account:
TCPASSWORDHASH=`openssl passwd -1`
replaceEscaped=$(sed 's/[&/\]/\\&/g' <<<"$TCPASSWORDHASH")
cat shadow.template | sed -e "s/INSERTPASSWORDHASHHERE/$replaceEscaped/" > shadow

echo '######################################################################'
echo '# Clean up any leftover files from previous build:'
echo '######################################################################'
rm -rf corepure64.gz isolinux remastered-corepure64.gz unpacked vmlinuz64 newiso TC-remastered.iso squashfs-root

echo '######################################################################'
echo '#  Fetch CorePure64 distribution ISO if necessary:'
echo '######################################################################'
[ -f CorePure64-current.iso ] || wget -q http://repo.tinycorelinux.net/$VERSION.x/x86_64/release/CorePure64-current.iso

echo '######################################################################'
echo '# Extract files from distribution ISO:'
echo '######################################################################'
[ -d /tmp/core64 ] || mkdir /tmp/core64
mount -t iso9660 -o ro,loop CorePure64-current.iso /tmp/core64
cp -r -a -p /tmp/core64/boot/* .
sync;sync;sync;
sleep 5
umount /tmp/core64
rmdir /tmp/core64

mkdir unpacked
cd unpacked
zcat ../corepure64.gz | cpio -i -H newc -d >/dev/null

echo '######################################################################'
echo '# Add contents of extrafiles directory:'
echo '######################################################################'
rsync -a ../extrafiles/ ../unpacked/

echo '######################################################################'
echo '# Add bootlocal.sh and shadow:'
echo '######################################################################'
cp ../bootlocal.sh opt/
cp ../shadow etc/

echo '######################################################################'
echo '# Add extra packages from extratczs.txt:'
echo '######################################################################'
mkdir -p opt/tce/optional
cd opt/tce/optional
URL=
TCZS=`cat ../../../../extratczs.txt`
for tcz in $TCZS
  do
      echo -n "$tcz..."
      if wget -q http://repo.tinycorelinux.net/$VERSION.x/x86_64/tcz/$tcz
        then
	  echo "OK."
      else
	  echo "Failed. Aborting"
	  exit
      fi
done

echo '######################################################################'
echo '# Create remastered compressed filesystem:'
echo '######################################################################'
cd ../../..
sh -c "find . | cpio -o -H newc | gzip -2" > ../remastered-corepure64.gz

cd ..
advdef -z4 remastered-corepure64.gz

echo '######################################################################'
echo '# Create isolinux configuration for new ISO image:'
echo '######################################################################'
sed -i -e 's/timeout 300/timeout 1/' isolinux/isolinux.cfg

echo '######################################################################'
echo '# Make new ISO image:'
echo '######################################################################'
mkdir -p newiso/boot
mv remastered-corepure64.gz newiso/boot/corepure64.gz
mv vmlinuz64 newiso/boot/
mv isolinux newiso/boot/
mkisofs -l -J -R -V TC-custom -no-emul-boot -boot-load-size 4  -boot-info-table -b boot/isolinux/isolinux.bin  -c boot/isolinux/boot.cat -o TC-remastered.iso newiso
isohybrid TC-remastered.iso

echo '######################################################################'
echo '# Create USB drive image, for EFI booting:'
echo '######################################################################'

# Create mount point:
mkdir /tmp/core64

echo '######################################################################'
echo '# Create empty disk image:'
echo '######################################################################'
dd if=/dev/zero of=tc-image.img bs=500M count=1
gdisk tc-image.img > /dev/null << EOF
o
y
n
1

+300M
0700
n
2

+200M
ef00
x
r
h
1 2
y

y

y

y
w
y
EOF

echo '######################################################################'
echo '# Make EFI partition legacy-bootable:'
echo '######################################################################'
sgdisk tc-image.img --attributes=2:set:2

echo '######################################################################'
echo '# Format partitions in image file:'
echo '######################################################################'

echo '######################################################################'
echo '# Partition for generic storage:'
echo '######################################################################'
START1=`fdisk -l tc-image.img | grep tc-image.img1 | awk '{print $2}'`
SECTORS1=`fdisk -l tc-image.img | grep tc-image.img1 | awk '{print $4}'`
LOOP=`losetup --offset $((512*$START1)) --sizelimit $((512*$SECTORS1)) --show --find tc-image.img`
if ( echo $LOOP | grep -q '^/dev/loop.$' 1> /dev/null 2>&1 )
  then
	mkfs.vfat $LOOP
else
	echo Problem making loopback. Aborting.
	exit
fi
losetup -d $LOOP

echo '######################################################################'
echo '# EFI partition:'
echo '######################################################################'
START2=`fdisk -l tc-image.img | grep tc-image.img2 | awk '{print $2}'`
SECTORS2=`fdisk -l tc-image.img | grep tc-image.img2 | awk '{print $4}'`
LOOP=`losetup --offset $((512*$START2)) --sizelimit $((512*$SECTORS2)) --show --find tc-image.img`
if ( echo $LOOP | grep -q '^/dev/loop.$' 1> /dev/null 2>&1 )
  then
      mkfs.vfat $LOOP
      udevadm control -s # Pause udev, because mtools (used by syslinux) has a race with it.
      syslinux -i $LOOP
      udevadm control -S # Resume udev
      mount $LOOP /tmp/core64
      mkdir -p /tmp/core64/EFI/BOOT
      unsquashfs ./unpacked/opt/tce/optional/syslinux.tcz
      cp ./squashfs-root/usr/local/share/syslinux/efi64/syslinux.efi /tmp/core64/EFI/BOOT/BOOTX64.EFI
      
      dd conv=notrunc if=./squashfs-root/usr/local/share/syslinux/gptmbr.bin of=tc-image.img bs=440 count=1
      
      cp ./squashfs-root/usr/local/share/syslinux/efi64/ldlinux.e64 /tmp/core64/
      cp ./squashfs-root/usr/local/share/syslinux/libutil.c32 /tmp/core64/
      cp ./squashfs-root/usr/local/share/syslinux/libcom32.c32 /tmp/core64/
      cp ./squashfs-root/usr/local/share/syslinux/vesamenu.c32 /tmp/core64/
      
      cp ./syslinux.cfg /tmp/core64/
      
      cp ./newiso/boot/corepure64.gz /tmp/core64/
      cp ./newiso/boot/vmlinuz64 /tmp/core64/
      sync;sync;sync
      sleep 5
      umount /tmp/core64
else
	echo Problem making loopback. Aborting.
	exit
fi
losetup -d $LOOP

echo '######################################################################'
echo '# Cleanup '
echo '######################################################################'
rmdir /tmp/core64

echo '######################################################################'
echo '# Done. '
echo '######################################################################'
