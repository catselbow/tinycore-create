#!/bin/sh

SOURCE=$1
if [ "$SOURCE" == "" ]
  then
    echo Must supply source.
    exit
fi

echo "NOTE! This script will trash this computer. Press enter to continue, or Ctrl-C to quit."
read ans
echo "One more chance to quit..."
read ans

# Delete partitions, create new ones:
gdisk /dev/nvme0n1 <<EOF
o
y
n
1

+1024M
ef00
n
2

+1024M
8300
n
3


8e00
w
y
EOF

sleep 1
sync;sync;sync

pvcreate -y -ff /dev/nvme0n1p3
vgcreate almalinux /dev/nvme0n1p3
lvcreate -y -L1024M -n swap almalinux
lvcreate -y -l100%FREE -n root almalinux

mkfs.vfat /dev/nvme0n1p1
mkfs.xfs -f -L/boot /dev/nvme0n1p2
mkfs.xfs -f -L/ /dev/almalinux/root
mkswap /dev/almalinux/swap

mkdir /mnt/tmp
mount /dev/almalinux/root /mnt/tmp
mkdir /mnt/tmp/boot
mount /dev/nvme0n1p2 /mnt/tmp/boot
mkdir /mnt/tmp/boot/efi
mount /dev/nvme0n1p1 /mnt/tmp/boot/efi

# tiny core's rsync doesn't deal well with -A or -X.  Do this in two stages:
rsync -av -x --numeric-ids --exclude=/home --exclude=/var/spool/mail $SOURCE:/boot/ /mnt/tmp/boot/
rsync -av -x --numeric-ids --exclude=/home --exclude=/var/spool/mail $SOURCE:/ /mnt/tmp/

echo Press enter to continue...
read ans

mount -o bind /dev /mnt/tmp/dev
mount -t sysfs sys /mnt/tmp/sys
mount -t proc none /mnt/tmp/proc

# Second stage of rsync:
chroot /mnt/tmp rsync -av -x -A -X --numeric-ids --exclude=/home --exclude=/var/spool/mail $SOURCE:/boot/ /boot/
chroot /mnt/tmp rsync -av -x -A -X --numeric-ids --exclude=/home --exclude=/var/spool/mail $SOURCE:/ /

echo Press enter to continue...
read ans

# Need to set date, or else dnf/curl will complain:
ntpd -p ntp1.virginia.edu

# Set up grub for efi booting:
chroot /mnt/tmp dnf -y install grub2-efi-x64 grub2-efi-x64-modules shim-x64
chroot /mnt/tmp dnf -y reinstall grub2-efi-x64 grub2-efi-x64-modules shim-x64
chroot /mnt/tmp /sbin/grub2-mkconfig -o /boot/efi/EFI/almalinux/grub.cfg

# Find out which kernel is installed:
KERNEL=`chroot /mnt/tmp rpm -q kernel | sort | tail -1 | sed -e 's/kernel-//'`

# Rebuild initramfs
chroot /mnt/tmp dracut --force /boot/initramfs-$KERNEL.img $KERNEL

echo Press enter to reboot, or ctrl-c to quit...
read ans

sync;sync;sync
reboot

