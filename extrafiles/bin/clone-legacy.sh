#!/bin/sh

SOURCE=$1
if [ "$SOURCE" == "" ]
  then
    echo Must supply source.
    exit
fi

echo "NOTE! This script will trash this computer. Press enter to continue, or Ctrl-C to quit."
read ans

modprobe xfs

# Need to add appropriate commands to delete and create partitions:
gdisk <<EOF

q
EOF

pvcreate /dev/sda2
vgcreate almalinux /dev/sda2
lvcreate -L1024M -n swap almalinux
lvcreate -l100%FREE -n root almalinux

mkfs.xfs -L/boot /dev/sda1
mkfs.xfs -L/ /dev/almalinux/root
mkswap /dev/almalinux/swap

mkdir /mnt/tmp
mount /dev/almalinux/root /mnt/tmp
mkdir /mnt/tmp/boot
mount /dev/sda1 /mnt/tmp/boot

# tiny core's rsync doesn't deal well with -A or -X.  Do this in two stages:
rsync -av -x --numeric-ids --exclude=/home --exclude=/var/spool/mail $SOURCE:/boot/ /mnt/tmp/boot/
rsync -av -x --numeric-ids --exclude=/home --exclude=/var/spool/mail $SOURCE:/ /mnt/tmp/

mount -o bind /dev /mnt/tmp/dev
mount -t sysfs sys /mnt/tmp/sys
mount -t proc none /mnt/tmp/proc

# Second stage of rsync:
chroot /mnt/tmp rsync -av -x -A -X --numeric-ids --exclude=/home --exclude=/var/spool/mail $SOURCE:/boot/ /boot
chroot /mnt/tmp rsync -av -x -A -X --numeric-ids --exclude=/home --exclude=/var/spool/mail $SOURCE:/ /

chroot /mnt/tmp /sbin/grub2-mkconfig -o /boot/grub2/grub.cfg
chroot /mnt/tmp /sbin/grub2-install --recheck /dev/sda

sync;sync;sync
reboot

