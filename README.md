# Tinycore-Create

## Description:

A script and associated files for creating a customized Tiny Core corepure64 image. The script "make.sh" makes the image.  It makes use of several other files that configure the generated image.  To use it, do the following:

* Add the names of any extra tcz packages to extratczs-template.txt. Note that packages that include the kernel version in their name can be referred to using the placeholder "KERNELVERSION", like:

  filesystems-KERNELVERSION-tinycore64.tcz

* Put any extra files under the extrafiles directory. For example, you might put useful scripts under extrafiles/bin.  The contents of extrafiles will be rsync'ed with / within the created tiny core image.

* Modify the bootlocal.sh.template to your taste. This will be the basis for the bootlocal.sh post-startup script that will be installed in the tiny core image. The template includes placeholders named MYEMAILADDRESS, MYSMTPSERVER, and TCPASSWORDHASH that will be filled in by make.sh when creating bootlocal.sh

* Set the VERSION variable inside make.sh to select the major tinycore version you want to use. Note that the selection of tczs in extratczs-template.txt needs to be compatible with this version.

* type "sh make.sh".  This will create three things:

  - A pair of files in the directory newiso/boot, **vmlinuz64** and **corepure64.gz**, which are a kernel and an initrd that can be used with a bootloader or through PXE.
  
  - An ISO file named **TC-remastered.iso**. This can be written to a CD or a USB drive that can be booted in "legacy" mode.
  
  - An image file named **tc-image.img**. This can be written onto a USB drive (at least 1GB big) and can then be booted with either UEFI or in "legacy" mode.
  
